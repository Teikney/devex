<!DOCTYPE html>
<html lang="en" class="h-full bg-indigo-50">
    <head>
        <meta charset="UTF-8" />
        {{--<link rel="icon" href="/favicon.ico" />--}}
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Termo de Entrega/Devolução de Equipamentos</title>
    </head>
    <body>
    <div class="grid grid-cols-3 grid-rows-1 gap-3">
        <img src="{{ asset('imgs/republica_portuguesa-saude.png') }}" alt="República Portuguesa - Saúde">
        <img src="{{ asset('imgs/SNS.png') }}" alt="SNS - Serviço Nacional de Saúde">
        <img src="{{ asset('imgs/ars_algarve.png') }}" alt="ARS Algarve">
    </div>
    <div class="grid grid-rows-10 grid-cols-1 my-2 ml-4">
        <div class="grid grid-rows-1 grid-cols-3 place-items-center">
            <img src="{{ asset('imgs/ars_algarve.png') }}" alt="ARS Algarve">
            {{--<img class="m-auto col-span-1" src="imgs/ars_algarve.png" alt="ARS Algarve"/>--}}
            <div class="col-span-2 text-center">
                <h2 class="text-xl font-bold"> Termo de Responsabilidade</h2>
                <p class="text-lg font-semibold -pr-10">Ligação de Equipamentos</p>
            </div>
        </div>
        <div class="text-sm text-justify mx-4">
            Através da assinatura deste documento, comprometo-me a seguir as normas descritas abaixo para, a partir da data de hoje, possibilitar a ligação do <b>EQUIPAMENTO</b>, na rede da ARS Algarve, IP.
        </div>
        <div class="grid grid-cols-2 grid-rows-3 gap-x-2 my-4 mx-2 text-sm">
            <div class="border-b border-black pt-1 font-semibold">Marca:</div>
            <div class="border-b border-black pt-1 font-semibold">Modelo:</div>
            <div class="border-b border-black pt-1 font-semibold">Nº de Série:</div>
            <div class="border-b border-black pt-1 font-semibold">Mac Address:</div>
            <div class="border-b border-black pt-1 font-semibold">Tipo:</div>
        </div>
        <div class="grid grid-rows-3 grid-cols-4 mx-2 gap-2 text-sm">
            <div class="col-span-4 border-b border-black pt-1 font-semibold">
                Nome Completo:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                B.I/C.C.:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                Nº Mecan./NIF:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                VÍNCULO:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                Departamento/Unidade:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                Serviço/Sector:
            </div>
            <div class="col-span-2 border-b border-black pt-1 font-semibold">
                Telefone/Telemóvel:
            </div>
        </div>
        <div class="m-2">
            <h2 class="font-semibold">Normas:</h2>
            <ol class="list-decimal text-xs text-justify mx-6">
                <li>
                    <p>O utilizador é inteiramente responsável pela segurança dos dados no(s) equipamento(s) que sejam ligados por ele à rede;</p>
                </li>
                <li>
                    <p>O utilizador será responsável por quaisquer incidentes de segurança gerados ativamente ou passivamente pelo equipamento por ele ligado à rede;</p>
                </li>
                <li>
                    <p>O utilizador deverá observar a política de segurança da informação da ARS Algarve, utilizando a rede da ARS Algarve somente para o desempenho das funções previstas. A utilização de programas de troca ou <i>download </i>de ficheiros do tipo P2P (<i>Bit Torrent, E-mule </i>e semelhantes) é e xpressame nte proibida;</p>
                </li>
                <li>
                    <p>Caso a utilização do equipamento resulte em transtorno para a estrutura da rede da ARS Algarve, o utilizador será responsável pelo ocorrido e terá o equipamento removido da rede da ARS Algarve, além de sofrer as sanções administrativas e legais que a ARS Algarve julgar cabíveis;</p>
                </li>
                <li>
                    <p>O utilizador é responsável pelo licenciamento dos softwares utilizados no(s) seu(s) equipamento(s), sendo vedada a instalação de software s cujas licenças pertençam à ARS Algarve, salvo em caso de exceções previamente autorizadas pelo Conselho Directivo;</p>
                </li>
                <li>
                    <p>A manutenção do equipamento e do software contido no mesmo é de total responsabilidade do ut ilizador, não cabendo a ARS Al garve ne nhuma responsabilidade referente ao suporte dos mesmos;</p>
                </li>
                <li>
                    <p>A criação e manutenção de cópias de segurança (<i>backup</i>) dos ficheiros contidos no equipamento são de inteira responsabilidade do utilziador;</p>
                </li>
                <li>
                    <p>A ARS Algarve, IP se reserva o direito de cancelar o acesso fornecido a qualquer momento sem aviso prévio;</p>
                </li>
                <li>
                    <p>O utilizador deverá possuir software de antivírus instalado, ativoe atualizadono equipamento a ser l igado na re de, al ém de manter todas as correções de segurança do sistema operacional;</p>
                </li>
            </ol>
        </div>
        <div>
            Para funcionários
        </div>
        <div>
            Para visitantes
        </div>
        <div>
            Assinaturas
        </div>
        <div>
            Justificação
        </div>
        <div class="grid grid-cols-2 grid-rows-1 text-start text-sm">
            <div>

            </div>
            <p>
                Informações para controle interno: emissão Março/2011 – Actualização 06/07/2012
                <br/>
                Núcleo de Sistemas de Informação e Comunicação
                <br/>
                Largo do Carmo, Nº 7 8000-148 Faro
                <br/>
                Tlf: (+351) 289889900 – Fax (+351) 289889909
                <br/>
                E-mail: nsic@arsalgarve.min-saude.pt
                <br/>
                www.arsalgarve.min-saude.pt
            </p>
        </div>

    </div>
    <table>
        <tr>
            <td>
                imagem

            </td>
        </tr>
    </table>
    <h1 style="padding-top: 2pt;padding-left: 259pt;text-indent: 0;text-align: center;">Termo de Responsabilidade</h1>
    <p class="s1" style="padding-left: 259pt;text-indent: 0;text-align: center;">Ligação de equipamentos</p>
    <p style="text-indent: 0;text-align: left;">
        <br/>
    </p>
    <p class="s2" style="padding-top: 3pt;padding-left: 6pt;text-indent: 0;text-align: left;">Através da assinatura deste documento, comprometo-me a seguir as normas descritas abaixo para, a partir da data de hoje, possibilitar a ligação do <b>EQUIPAMENTO</b>, na rede da ARS Algarve, IP.</p>
    <p style="text-indent: 0;text-align: left;">
        <br/>
    </p>
    <h2 style="padding-left: 6pt;text-indent: 0;text-align: left;">
        Marca:<span class="s3">.................................................. </span>
        Modelo:<span class="s3">.............................................. </span>
        Nº de Série:<span class="s3">.............................................................</span>
    </h2>
    <h2 style="padding-top: 2pt;padding-left: 6pt;text-indent: 0;text-align: left;">
        Mac Address: <span class="s3">......................................................................................</span>
    </h2>
    <p style="text-indent: 0;text-align: left;">
        <br/>
    </p>
    <p class="s2" style="padding-top: 3pt;padding-left: 24pt;text-indent: 0;text-align: left;">Portátil</p>
    <p class="s2" style="padding-top: 3pt;padding-left: 24pt;text-indent: 0;text-align: left;">Telemóvel</p>
    <p class="s2" style="padding-top: 3pt;padding-left: 14pt;text-indent: 0;text-align: left;">Black Berry</p>
    <p class="s2" style="padding-top: 3pt;padding-left: 24pt;text-indent: 0;text-align: left;">Palm Top</p>
    <p class="s2" style="padding-top: 3pt;padding-left: 24pt;text-indent: 0;text-align: left;">Desktop</p>
    <p class="s2" style="padding-top: 3pt;padding-left: 24pt;text-indent: 0;text-align: left;">Outros....................................................................</p>
    <h2 style="padding-top: 8pt;padding-left: 6pt;text-indent: 0;text-align: left;">NOME COMPLETO:</h2>
    <p style="text-indent: 0;text-align: left;"><br/></p>

    <ol id="l1">
        <li>
            <h2 style="padding-top: 3pt;padding-left: 20pt;text-indent: -14pt;text-align: left;">/C.C.:                               Nº Mecan./NIF :</h2>
            <h2 style="padding-top: 3pt;padding-left: 6pt;text-indent: 0;text-align: left;">VÍNCULO:</h2>
            <h2 style="padding-top: 7pt;padding-left: 6pt;text-indent: 0;line-height: 162%;text-align: left;">Departam/Unid: Serviço/sector:</h2>
            <p class="s2" style="padding-top: 3pt;padding-left: 6pt;text-indent: 0;text-align: left;">Funcionário</p>
            <p class="s2" style="padding-top: 3pt;padding-left: 6pt;text-indent: 0;text-align: left;">Estagiário</p>
            <p class="s2" style="padding-top: 4pt;padding-left: 6pt;text-indent: 0;text-align: left;">Outros..........................................</p>
            <h2 style="padding-left: 6pt;text-indent: 0;text-align: left;">Telefone:</h2>
            <h2 style="padding-left: 6pt;text-indent: 0;text-align: left;">TLM:</h2>
            <h4 style="padding-top: 5pt;padding-left: 24pt;text-indent: 0;text-align: left;">Normas<span class="s4">:</span></h4>
            <ol id="l3">
                <li>
                    <p style="padding-top: 5pt;padding-left: 42pt;text-indent: -18pt;text-align: justify;">O utilizador é inteiramente responsável pela segurança dos dados no(s) equipamento(s) que sejam ligados por ele à rede;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;line-height: 10pt;text-align: justify;">O utilizador será responsável por quaisquer incidentes de segurança gerados ativamente ou passivamente pelo equipamento por ele ligado à rede;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">O utilizador deverá observar a política de segurança da informação da ARS Algarve, utilizando a rede da ARS Algarve somente para o desempenho das funções previstas. A utilização de programas de troca ou <i>download </i>de ficheiros do tipo P2P (<i>Bit Torrent, E-mule </i>e semelhantes) é e xpressame nte proibida;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">Caso a utilização do equipamento resulte em transtorno para a estrutura da rede da ARS Algarve, o utilizador será responsável pelo ocorrido e terá o equipamento removido da rede da ARS Algarve, além de sofrer as sanções administrativas e legais que a ARS Algarve julgar cabíveis;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">O utilizador é responsável pelo licenciamento dos softwares utilizados no(s) seu(s) equipamento(s), sendo vedada a instalação de software s cujas licenças pertençam à ARS Algarve, salvo em caso de exceções previamente autorizadas pelo Conselho Directivo;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">A manutenção do equipamento e do software contido no mesmo é de total responsabilidade do ut ilizador, não cabendo a ARS Al garve ne nhuma responsabilidade referente ao suporte dos mesmos;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;line-height: 10pt;text-align: justify;">A criação e manutenção de cópias de segurança (<i>backup</i>) dos ficheiros contidos no equipamento são de inteira responsabilidade do utilziador;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">A ARS Algarve, IP se reserva o direito de cancelar o acesso fornecido a qualquer momento sem aviso prévio;</p>
                </li>
                <li>
                    <p style="padding-left: 42pt;text-indent: -18pt;text-align: justify;">O utilizador deverá possuir software de antivírus instalado, ativoe atualizadono equipamento a ser l igado na re de, al ém de manter todas as correções de segurança do sistema operacional;</p>
                </li>
            </ol>
        </li>
    </ol>
    <table style="border-collapse:collapse;margin-left:5.675pt">
        <tr style="height:51pt">
            <td style="width:532pt;border-top-style:solid;border-top-width:1pt;border-top-color:#808080;border-left-style:solid;border-left-width:1pt;border-left-color:#808080;border-bottom-style:solid;border-bottom-width:1pt;border-bottom-color:#808080;border-right-style:solid;border-right-width:1pt;border-right-color:#808080">
                <p class="s6" style="padding-top: 5pt;padding-left: 5pt;text-indent: 0;text-align: left;">Para funcionários</p>
                <p class="s7" style="padding-top: 6pt;padding-left: 106pt;padding-right: 105pt;text-indent: 0;text-align: center;">........................................................ .................................................................................</p>
                <p class="s7" style="padding-left: 106pt;padding-right: 104pt;text-indent: 0;text-align: center;">Assinatura               Endereço de e-mail</p>
            </td>

        </tr>
        <tr style="height:51pt">
            <td style="width:532pt;border-top-style:solid;border-top-width:1pt;border-top-color:#808080;border-left-style:solid;border-left-width:1pt;border-left-color:#808080;border-bottom-style:solid;border-bottom-width:1pt;border-bottom-color:#808080;border-right-style:solid;border-right-width:1pt;border-right-color:#808080">
                <p class="s6" style="padding-top: 1pt;padding-left: 5pt;text-indent: 0;text-align: left;">Para visitantes</p>
                <p class="s7" style="padding-left: 52pt;text-indent: 0;text-align: left;">Assinatura                   Endereço de e-mail              Data de término p/ visitantes</p>
            </td>
        </tr>
        <tr style="height:55pt">
            <td style="width:532pt;border-top-style:solid;border-top-width:1pt;border-top-color:#808080;border-left-style:solid;border-left-width:1pt;border-left-color:#808080;border-bottom-style:solid;border-bottom-width:1pt;border-bottom-color:#808080;border-right-style:solid;border-right-width:1pt;border-right-color:#808080">
                <p style="text-indent: 0;text-align: left;">
                    <br/>
                </p>
                <p class="s7" style="padding-left: 5pt;text-indent: 0;text-align: left;">..................................................................    ........................    ...............................................................   carimbo da Dep/Unidade</p>
                <p class="s7" style="padding-left: 5pt;text-indent: 0;text-align: left;">Nome do Dirct/coord Dep / unidade      Nº Mecanográfico          Assinatura</p>
            </td>
        </tr>
        <tr style="height:89pt">
            <td style="width:532pt;border-top-style:solid;border-top-width:1pt;border-top-color:#808080;border-left-style:solid;border-left-width:1pt;border-left-color:#808080;border-bottom-style:solid;border-bottom-width:1pt;border-bottom-color:#808080;border-right-style:solid;border-right-width:1pt;border-right-color:#808080">
                <p style="text-indent: 0;text-align: left;"><br/></p>
                <p class="s7" style="padding-left: 5pt;text-indent: 0;text-align: left;">Justificação: ..............................................................................................................................................................................................................<br/>................................................................................................................................................................................................................................<br/>.....................................................................................................................................................................................................................................<br/>.....................................................................................................................................................................................................................................<br/>.....................................................................................................................................................................................................................................</p>
                <p class="s7" style="padding-left: 350pt;text-indent: 0;line-height: 10pt;text-align: left;">
                    Faro,
                </p>
            </td>
        </tr>
    </table>
    <p style="padding-top: 5pt;padding-left: 253pt;text-indent: 0;text-align: left;">Informações para controle interno: emissão Março/2011 – Actualização 06/07/2012</p>
    <p class="s9" style="padding-top: 4pt;padding-left: 265pt;text-indent: 0;text-align: left;">Núcleo de Sistemas de Informação e Comunicação</p>
    <p class="s10" style="padding-left: 265pt;text-indent: 0;line-height: 107%;text-align: left;">Largo do Carmo, Nº 7 8000-148 Faro</p>
    <p class="s10" style="padding-left: 265pt;text-indent: 0;line-height: 8pt;text-align: left;">Tlf: (+351) 289889900 – Fax (+351) 289889909</p>
    <p style="padding-left: 265pt;text-indent: 0;text-align: left;">
        <a href="mailto:nsic@arsalgarve.min-saude.pt" class="s11" target="_blank">E-mail: </a>
        <a href="mailto:nsic@arsalgarve.min-saude.pt" target="_blank">nsic@arsalgarve.min-saude.pt</a>
    </p>
    <p style="padding-left: 265pt;text-indent: 0;text-align: left;">
        <a href="https://www.arsalgarve.min-saude.pt/" class="s11">www.arsalgarve.min-saude.pt</a>
    </p>
    </body>
</html>
