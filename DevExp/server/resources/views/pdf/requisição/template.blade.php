<html lang="EN-US">
    <head>
        <meta http-equiv=Content-Type content="text/html" charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>TERMO DE EMPRÉSTIMO/DEVOLUÇÃO</title>
        <link type="text/css" href="css/app.css" rel="stylesheet" />
        <script src="js/app.js"></script>
        <style>
            @page {
                margin: 0;
            }

            *,
            *:before,
            *:after {
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            td, th {
                border: 1px solid #000000;
                padding: 1px 2px;
            }
            th {
                background-color: #fafafa;
            }


        </style>
    </head>
    <body>
        <div class="m-10">
            <!-- Imagens -->
            <span class="grid text-center">
                <img src="imgs/republica_portuguesa-saude.png" alt="República Portuguesa - Saúde" width="25%" height="25%">
                <img class="mx-5" src="imgs/SNS.png" alt="SNS - Serviço Nacional de Saúde" width="40%" height="40%">
                <img src="imgs/ars_algarve.png" alt="ARS Algarve" width="20%" height="20%">
            </span>
            <!-- Título -->
            <div class="mb-2 py-3 font-bold text-md text-center" style="border: 1px solid #000000;background-color: #dadada">
                TERMO DE ENTREGA/DEVOLUÇÃO DE PORTATÉIS
            </div>

            <!-- Dados User -->
            <table class="font-base text-xs" style="width: 100%;">
                <tr>
                    <th>Funcionário:</th>
                    <td>{{$requisition->user->name}}</td>
                    <th>Nº Mecan.:</th>
                    <td style="text-align: right">{{$requisition->user->num_mecanografico}}</td>
                    <th>Previsão de Devolução:</th>
                    <td style="text-align: right">{{date('d-m-Y', strtotime($requisition->expire_date)) ?? '___/___/______'}}</td>
                </tr>
                <tr style="width: 100%">
                    <th>Instituição:</th>
                    <td colspan="5">{{$requisition->user->workspace ?? 'Administração Regional da Saúde do Algarve'}}</td>
                </tr>
            </table>

            <br>

            <!-- Dados Equipamentos -->
            <table class="mt-1 font-base text-xs" style="border-collapse: collapse;width: 100%;">
                <tr style="width: 100%;">
                    <th colspan="5" style="text-align: left">
                        Equipamentos
                    </th>
                </tr>
                <tr>
                    <th>
                        Qt.
                    </th>
                    <th>
                        Tipo
                    </th>
                    <th>
                        Marca/Modelo
                    </th>
                    <th>
                        Património
                    </th>
                    <th>
                        Nº Série
                    </th>
                </tr>
                @foreach($requisition->devices as $device)
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        {{--{{ $device->type . " " . $device->item }}--}}
                        {{ucfirst($device->extractDevicesTypeAndItem())}}
                    </td>
                    <td>
                        {{$device->brand . "/" . $device->model}}
                    </td>
                    <td style="text-align: right">
                        {{$device->ars_number}}
                    </td>
                    <td style="text-align: right">
                        {{$device->serial_number}}
                    </td>
                </tr>
                @endforeach
            </table>

            <!-- Termo de Responsabilidade -->
            <table class="mt-1 font-base text-xs" style="border-collapse: collapse;width: 100%;">
                <tr>
                    <th class="text-sm">
                        Termo de Responsabilidade
                    </th>
                </tr>
                <tr>
                    <td>
                        <p class="text-justify text-center text-xs px-4 font-base indent-4">
                            Pelo presente Termo de Entrega e Responsabilidade, com a identificação acima mencionada, declara que recebeu o equipamento e acessórios acima especificados, de propriedade da ARS Algarve, IP,, assumindo o compromisso de manter a guarda pessoal sobre os mesmos, <b>ficando a seu cargo</b>:
                        </p>
                        <ul class="list-disc ml-12 mr-4 text-xs mb-1 text-justify">
                            <li>
                                Total responsabilidade por extravios ou danos bem como pela legalidade dos ‘softwares’ nele instalado sem o consentimento da instituição;
                            </li>
                            <li>
                                Adequada utilização, conforme as recomendações;
                            </li>
                            <li>
                                Comunicar, imediatamente, qualquer incidente e ocorrência (incluídos ataques maliciosos/vírus) com o equipamento sob a sua guarda e responsabilidade;
                            </li>
                            <li>
                                Devolução do referido equipamento logo que cessarem as minhas atividades.
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>

            <!-- Assinaturas Requisição -->
            <table class="mt-1 text-center" style="width: 100%;">
                <tr>
                    <th colspan="3" class="text-sm">
                        Rúbrica de Requisição
                    </th>
                </tr>
                <tr class="font-base text-xs">
                    <td style="width: 20%;">
                        Data: {{ date('d-m-Y', strtotime($requisition->deliver_at)) ?? '___/___/______' }}
                    </td>
                    <td class="pl-1" style="text-align: left;padding: 4px 8px">
                        Funcionário:
                    </td>
                    <td class="pl-1" style="text-align: left">
                        Colaborador NSIC:
                    </td>
                </tr>
            </table>

            <br>

            <!-- Termo de Devolução -->
            <table class="mt-1 font-base text-xs" style="border-collapse: collapse;width: 100%;">
                <tr>
                    <th class="text-sm">
                        Termo de Devolução
                    </th>
                </tr>
                <tr>
                    <td>
                        <p class="text-justify text-center text-xs px-4 font-base indent-4">Pelo presente Termo de Devolução, com a identificação acima mencionada declara que devolveu o equipamento e acessórios acima especificados, nas mesmas condições que os recebeu.</p>
                        <p class="text-justify text-center text-xs px-4 font-base indent-4">O colaborador/funcionário do NSIC, abaixo identificado, declara que recebeu os equipamentos em devolução, nas mesmas condições de acima assumidas.</p>
                    </td>
                </tr>
                <tr>
                    <td class="text-start" style="padding-bottom: 80px; padding-left: 6px">
                        Observações:
                    </td>
                </tr>
            </table>

            <!-- Assinaturas Devolução -->
            <table class="mt-1 text-center" style="width: 100%;">
                <tr>
                    <th colspan="3" class="text-sm">
                        Rúbrica de Devolução
                    </th>
                </tr>
                <tr class="font-base text-xs">
                    <td style="width: 20%;">
                        Data: ___/___/______
                    </td>
                    <td class="pl-1" style="text-align: left;padding: 4px 8px">
                        Funcionário:
                    </td>
                    <td class="pl-1" style="text-align: left">
                        Colaborador NSIC:
                    </td>
                </tr>
            </table>
        </div>
        <!-- Footer -->
        <footer class="text-center mt-2 font-semibold text-sm my-0.5">
            E.N. 125 Sítio das Figuras, Lote 1, 2ºandar, 8005-145 Faro <br> Telf:289889900 - Fax: 289813963 <br> email: servicedesk@arsalgarve.min-saude.pt  -  www.arsalgarve.min-saude.pt
        </footer>
    </body>

</html>
