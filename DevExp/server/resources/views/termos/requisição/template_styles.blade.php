<html lang="EN-US">
<head>
    <meta http-equiv=Content-Type content="text/html;text/css charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>TERMO DE EMPRÉSTIMO/DEVOLUÇÃO</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" media="all" />
    {{--<style>
        @page { margin: 0;}
    </style>--}}
</head>
<body>
<div class="my-2 grid grid-rows-9 grid-cols-1 ">
    <!-- Imagens -->
    <div class="px-8 py-1 grid grid-rows-1 grid-cols-3">
        <div class="m-auto">
            <img class="m-auto" src="{{asset('imgs/republica_portuguesa-saude.png')}}" alt="República Portuguesa - Saúde" width="25%" height="25%">
        </div>
        <div class="m-auto">
            <img class="m-auto" src="{{asset('imgs/SNS.png')}}" alt="SNS - Serviço Nacional de Saúde" width="30%" height="30%">
        </div>
        <div class="m-auto">
            <img class="m-auto" src="{{asset('imgs/ars_algarve.png')}}" alt="ARS Algarve" width="20%" height="20%">
        </div>
    </div>

    <!-- Título -->
    <div class="font-bold text-center text-md mx-10 my-2 border border-black bg-gray-400/40 py-3 my-0.5">
        TERMO DE ENTREGA/DEVOLUÇÃO DE PORTATÉIS
    </div>

    <!-- Dados User -->
    <div class="grid grid-cols-8 grid-rows-2 text-start text-sm border border-black mx-10 my-0.5">
        <div class="font-semibold col-span-6 px-1">
            Nome do profissional/requisitante
        </div>
        <div class="font-semibold col-span-2 border-l border-black">
            Nº Mecanográfico
        </div>
        <div class="font-semibold col-span-6 border-t border-black">
            Unidade/Serviço
        </div>
        <div class="font-semibold col-span-2 border-l border-t border-black">
            Data Prevista para Devolução
        </div>
        <div>

        </div>
    </div>

    <!-- Dados Equipamentos -->
    <div class="grid grid-cols-1 grid-rows-2 font-semibold text-sm border border-black mx-10 my-0.5">
        <div class="text-start bg-gray-400/20 max-h-8">
            Especificação do Equipamento
        </div>
        <div class="grid grid-rows-1 grid-cols-2 text-start">
            <div>
                Marca/Modelo
            </div>
            <div class="grid grid-rows-1 grid-cols-2 text-start">
                <div>
                    Património
                </div>
                <div>
                    Nº de Série
                </div>
            </div>
        </div>
        <div class="grid grid-rows-1 grid-cols-4 border-t border-black">
            <div class="col-span-1">
                Acessórios?
            </div>
            <div class="col-span-3">
                <div class="grid grid-cols-1 grid-rows-4">
                    <div class="border-b border-black py-3">

                    </div>
                    <div class="border-b border-black">

                    </div>
                    <div class="border-b border-black">

                    </div>
                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Termo de Responsabilidade -->
    <div class="border border-black mx-10 my-0.5">
        <h2 class="text-center text-md font-semibold text-gray-900 pb-1">Termo de Responsabilidade</h2>
        <p class="text-justify px-5 text-sm">
            Pelo presente Termo de Entrega e Responsabilidade, com a identificação acima mencionada, declara que recebeu o equipamento e acessórios acima especificados, de propriedade da ARS Algarve, IP,, assumindo o compromisso de manter a guarda pessoal sobre os mesmos, <b>ficando a seu cargo</b>:
        </p>
        <ul class="list-disc ml-12 md:ml-16 text-sm">
            <li>
                Total responsabilidade por extravios ou danos bem como pela legalidade dos softwares nele instalado sem o consentimento da instituição;
            </li>
            <li>
                Adequada utilização, de acordo com as recomendações;
            </li>
            <li>
                Comunicar, imediatamente, qualquer incidente e ocorrência (incluídos ataques maliciosos/vírus) com o equipamento sob sua guarda e responsabilidade;
            </li>
            <li>
                Devolução do referido equipamento logo que cessarem as minhas atividades.
            </li>
        </ul>
    </div>

    <!-- Assinaturas Requisição -->
    <div class="grid grid-cols-3 grid-rows-1 font-semibold text-sm text-center border border-black mx-10 my-0.5">
        <div class="mb-10">
            Data de Entrega
        </div>
        <div class="mb-10">
            Assinatura/Carimbo Profissional/Requisitante
        </div>
        <div class="mb-10">
            Assinatura e Nº Mecanográfico <br> Colaborador NSIC
        </div>
    </div>

    <!-- Termo de Devolução -->
    <div class="grid grid-cols-1 grid-rows-2 border border-black mx-10 my-0.5">
        <div class="mx-8">
            <h2 class="text-center text-md font-semibold text-gray-900 pb-1">Termo de Devolução</h2>
            <p class="text-sm">Pelo presente Termo de Devolução, com a identificação acima mencionada declara que devolveu o equipamento e acessórios acima especificados, nas mesmas condições que os recebeu.</p>
            <p class="text-sm">O colaborador/funcionário do NSIC, abaixo identificado, declara que recebeu os equipamentos em devolução, nas mesmas condições de acima assumidas.</p>
        </div>
        <div class="border-t border-black font-semibold text-sm">
            Observações
        </div>
    </div>

    <!-- Assinaturas Devolução -->
    <div class="grid grid-cols-3 grid-rows-1 font-semibold text-sm text-center border border-black mx-10 my-0.5">
        <div class="mb-10">
            Data de Devolução
        </div>
        <div class="mb-10">
            Assinatura/Carimbo Profissional/Requisitante
        </div>
        <div class="mb-10">
            Assinatura e Nº Mecanográfico <br> Colaborador NSIC
        </div>
    </div>

    <!-- Footer -->
    <div class="text-center mt-2 font-semibold text-sm my-0.5">
        E.N. 125 Sítio das Figuras, Lote 1, 2ºandar, 8005-145 Faro <br> Telf:289889900 - Fax: 289813963 <br> email: servicedesk@arsalgarve.min-saude.pt  -  www.arsalgarve.min-saude.pt
    </div>

</div>
</body>

</html>
