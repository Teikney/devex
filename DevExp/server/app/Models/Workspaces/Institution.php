<?php

namespace App\Models\Workspaces;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @method static truncate()
 * @method static create(array|false $array_combine)
 * @method static where(string $string, mixed $int)
 */
class Institution extends Model
{
    use HasFactory;

    public $timestamps = false;

    //Relationships

    public function workspace(): HasOne
    {
        return $this->hasOne(Workspace::class);
    }

    public function institution_type(): BelongsTo
    {
        return $this->belongsTo(InstitutionType::class);
    }

    public function county(): BelongsTo
    {
        return $this->belongsTo(County::class);
    }

}
