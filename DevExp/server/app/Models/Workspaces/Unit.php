<?php

namespace App\Models\Workspaces;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @method static create(array|false $array_combine)
 * @method static truncate()
 * @method static where(string $string, int $intval)
 * @method static select(string $string)
 */
class Unit extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'initials',
    ];

    //Relationships
    public function workspace() : HasOne {
        return $this->hasOne(Workspace::class);
    }

}
