<?php

namespace App\Models\Workspaces;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @method static create(array|false $array_combine)
 * @method static truncate()
 * @method static where(string $string, mixed $int)
 */
class County extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'zone',
        'code',
    ];

    //Relationships

    public function institution() : HasOne {
        return $this->hasOne(Institution::class);
    }
}
