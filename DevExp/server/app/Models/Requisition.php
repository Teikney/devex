<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @method static where(string $string, $id)
 * @method static create(array $validated)
 * @property mixed $user_id
 * @property mixed $id
 * @property mixed $devices
 */
class Requisition extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','status','code','expire_date','deliver_at','description','pdf'];

    protected $with = [
        'devices'
    ];
    public function devices(): HasMany
    {
        return $this->hasMany(Device::class);
    }

    public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }

    public function manager(): BelongsTo {
        return $this->belongsTo(User::class,'manager_id');
    }
}
