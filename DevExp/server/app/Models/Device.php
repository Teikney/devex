<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static where(string $string, $id)
 * @method static create(array $validated)
 * @method static paginate(int $int)
 * @property mixed $type
 * @property mixed $item
 * @property Requisition|mixed $requisition
 */
class Device extends Model
{
    use HasFactory;

    private $types = [
        'computador',
        'acessórios'
    ];

    private $items = [
        1 => [
            'fixo',
            'portátil'
        ],
        2 => [
            "monitor",
            "teclado",
            "rato",
            "câmara",
            "microfone",
            "colunas",
        ],
    ];

    const COMPUTER_TYPE = 1;
    const EXTRAS_TYPE = 2;

    const DESKTOP_ITEM = 1;
    const LAPTOP_ITEM = 2;
    const MONITOR_ITEM = 3;
    const KEYBOARD_ITEM = 4;
    const MOUSE_ITEM = 5;
    const WEBCAM_ITEM = 6;
    const MICROPHONE_ITEM = 7;
    const SOUND_ITEM = 8;

    protected $fillable = ['type','item','brand','model','serial_number','ars_number','requisition_id'];

    public function requisition(): BelongsTo
    {
        return $this->belongsTo(Requisition::class);
    }

    public function extractDevicesTypeAndItem(): string
    {

        if ($this->type === '1') {    //Significa que se trata de um COMPUTADOR
            $item = $this->items[$this->type][$this->item-1];
        }
        else {                      //Significa que se trata de um ACESSÓRIO
            $item = $this->items[$this->type][$this->item-3];
        }
        return  $this->type === '1' ? $this->types[$this->type - 1] . " " . $item : $item;


    }
}
