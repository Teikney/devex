<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserResponsibleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            "user_id" => $this->user()->id
        ]);
    }

    private array $vinculo = [
        "Estagiário",
        "Funcionário",
        "Outro"
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "type" => "required|integer|numeric|between:1,4",
            "other_type" => "prohibited_unless:type,4",
            "brand" => "required|string",
            "model" => "required|string",
            "serial" => "required|alpha_num",
            "macAddress" => "mac_address",
            "cc" => "prohibited_unless:bond,Outro|string|numeric|digits:8",
            "bond" => ["required", Rule::in($this->vinculo)],
            "other_bond" => "prohibited_unless:bond,Outro",
            "fname" => "required|string",
            "email" => "required|email",
            "num_mecanografico" => "prohibited_if:bond,Outro",
            "phone" => "required|digits:9|starts_with:+351,9",
            "institution" => "prohibited_if:bond,Outro|exists:workspaces,id",
            "unit" => "prohibited_if:bond,Outro|exists:units,id",

            "endDate" => "required|date|after:today",
            "justification" => "required|string",
            "user_id" => "required|exists:users,id",
        ];
    }
}
