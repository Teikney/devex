<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequisitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => $this->user()->id
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'description' => 'required|string',
            'deliver_at' => 'required|date|after:today',
            'expire_date' => 'required|date|after:tomorrow',
            'devices' => 'present|array',
            //'manager_id' => 'nullable|exists:users,',
            'user_id' => 'required|exists:users,id',
        ];
    }
}
