<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Password;
use Silber\Bouncer\BouncerFacade as Bouncer;


class AuthController extends \Illuminate\Routing\Controller
{
    public function register(Request $request) {
        $data = $request->validate([
            'num_mecanografico' => 'required|numeric|between:60000,99999',
            'name' => 'required|string',
            'email' => 'required|email|string|unique:users,email',
            'password' => [
                'required',
                'confirmed',
                Password::min(8)/*->mixedCase()->numbers()->symbols()*/
            ],
        ]);

        /** @var User $user */
        $user = User::create([
            'num_mecanografico' => $data['num_mecanografico'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        if ($user->id === 1) Bouncer::assign('admin');


        $token = $user->createToken('main')->plainTextToken;



        return response([
            'user' => $user,
            'token' => $token,
        ]);
    }

    public function login(Request $request) {

        $credentials = $request->validate([
            'num_mecanografico' => 'required|numeric|exists:users,num_mecanografico',
            'password' => [
                'required',
            ],
            'remember' => 'boolean'
        ]);
        $remember = $credentials['remember'] ?? false;
        unset($credentials['remember']);

        if (!Auth::attempt($credentials,$remember)) {
            return response([
                'error' => 'As credenciais não estão corretas.',
            ], 422);
        }

        $user = Auth::user();
        $token = $user->createToken('main')->plainTextToken;
        return response([
            'user' => $user,
            'token' => $token,
        ]);
    }

    public function logout() {
        /** @var User $user */
        $user = Auth::user();

        $user->currentAccessToken()->delete();

        return response(['success' => true]);
    }
}
