<?php

namespace App\Http\Controllers;

use App\Http\Resources\DeviceResource;
use App\Models\Device;
use App\Http\Requests\StoreDeviceRequest;
use App\Http\Requests\UpdateDeviceRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $user = $request->user();

        return DeviceResource::collection(Device::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDeviceRequest $request
     * @return DeviceResource
     */
    public function store(StoreDeviceRequest $request): DeviceResource
    {
        $result = Device::create($request->validated());


        return new DeviceResource($result);
    }

    /**
     * Display the specified resource.
     *
     * @param Device $device
     * @return DeviceResource
     */
    public function show(Device $device): DeviceResource
    {
        return new DeviceResource($device);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDeviceRequest $request
     * @param Device $device
     * @return DeviceResource
     */
    public function update(UpdateDeviceRequest $request, Device $device): DeviceResource
    {
        $device->update($request->validated());
        return new DeviceResource($device);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Device $device
     * @return Response
     */
    public function destroy(Device $device): Response
    {
        $device->delete();

        return response('',204);
    }
}
