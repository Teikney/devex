<?php

namespace App\Http\Controllers;

use App\Http\Resources\DeviceResource;
use App\Http\Resources\RequisitionResource;
use App\Http\Resources\RequisitionResourceDashboard;
use App\Models\Device;
use App\Models\Requisition;
use App\Models\User;
use Illuminate\Http\Request;
use Silber\Bouncer\Bouncer;

class DashboardController extends Controller
{
    public function index(Request $request): array {
        $user = $request->user();

        $totalRequisitions = Requisition::query()
            ->where('user_id',$user->id)
            ->count();

        $latestRequisition = Requisition::query()
            ->where('user_id',$user->id)
            ->latest('created_at')
            ->first();

        $totalDevices = Device::query()
            ->join('requisitions','devices.requisition_id','=','requisitions.id')
            ->where('requisitions.user_id',$user->id)
            ->count();

        //$latestDevices = Requisition::query()->join('devices','devices.requisition_id','requisitions.id')->where('')

        $latestDevices = Device::query()
            ->join('requisitions','devices.requisition_id','=','requisitions.id')
            ->where('requisitions.user_id',$user->id)
            ->orderBy('id','DESC')
            ->limit(5)
            ->getModels('devices.*');
        return [
            'total_requisitions' => $totalRequisitions,
            'latest_requisition' => $latestRequisition ?
                new RequisitionResourceDashboard($latestRequisition)
                : null,
            'total_devices' => $totalDevices,
            'latest_devices' => DeviceResource::collection($latestDevices),
        ];
    }

    public function getAbilities(Request $request) {
        $u = $request->user()->id;
        $user = User::query()->where('id',$u)->first();
        if (!$user) {
            abort(401,'Não autorizado.');
        }
        Bouncer::allow('admin')->to('manage_requisitions');
    }
}
