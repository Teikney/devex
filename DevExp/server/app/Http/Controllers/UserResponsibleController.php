<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserResponsibleRequest;
use App\Http\Resources\ResponsibleUserResource;
use App\Http\Resources\WorkspaceResource;
use App\Models\UserResponsible;
use App\Models\Workspaces\Workspace;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserResponsibleController extends Controller
{
    //
    public function index()
    {
        return UserResponsible::all();
    }

    public function show()
    {
        return UserResponsible::all();
    }

    public function store(StoreUserResponsibleRequest $request)
    {
        $data = $request->validated();
        if ($request->user()->id !== auth()->user()->id) return response('Unauthorized action', 404, ['Content-Type' => 'application/json']);

        //return response($data,200);
        $term = UserResponsible::create($data);
        return new ResponsibleUserResource($term);
    }

    public function getWorkspaces(): AnonymousResourceCollection
    {
        return WorkspaceResource::collection(
            Workspace::where('institution_id', 1)->with('unit')->get()
        //Unit::select('id','name')->get()
        );
    }
}
