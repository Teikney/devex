<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateRequisitionRequest;
use App\Http\Resources\RequisitionResource;
use App\Models\Device;
use App\Models\Requisition;
use App\Http\Requests\StoreRequisitionRequest;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class RequisitionController extends Controller
{

    private function devicesVerboseHandler($values)
    {
        //$values = $validator->validated();
        if ($values['type'] === 1) {
            if ($values['item'] === 1) {
                $values['type'] = 'computador';
                $values['item'] = 'fixo';
            } elseif ($values['item'] === 2) {
                $values['type'] = 'computador';
                $values['item'] = 'portátil';
            }
        } elseif ($values['type'] === 2) {
            if ($values['item'] === 3) {
                $values['type'] = 'acessórios';
                $values['item'] = 'monitor';
            } elseif ($values['item'] === 4) {
                $values['type'] = 'acessórios';
                $values['item'] = 'teclado';
            } elseif ($values['item'] === 5) {
                $values['type'] = 'acessórios';
                $values['item'] = 'rato';
            } elseif ($values['item'] === 6) {
                $values['type'] = 'acessórios';
                $values['item'] = 'câmara';
            } elseif ($values['item'] === 7) {
                $values['type'] = 'acessórios';
                $values['item'] = 'microfone';
            } elseif ($values['item'] === 8) {
                $values['type'] = 'acessórios';
                $values['item'] = 'colunas';
            }
        }
        return $values;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        $user = $request->user();

        return RequisitionResource::collection(Requisition::where('user_id', $user->id)->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequisitionRequest $request
     * @return RequisitionResource
     * @throws ValidationException
     */
    public function store(StoreRequisitionRequest $request): RequisitionResource
    {
        $data = $request->validated();

        $data['status'] = false;

        do {
            $data['code'] = uniqid();
        } while (Requisition::where('code', $data['code'])->count() > 0);

        $requisition = Requisition::create($data);

        //Create new devices
        foreach ($data['devices'] as $device) {
            $device['requisition_id'] = $requisition->id;
            $this->createDevice($device);
        }

        return new RequisitionResource($requisition);
    }

    /**
     * @throws ValidationException
     */
    private function createDevice($data): void
    {
        $validator = Validator::make($data, [
            'ars_number' => 'nullable|string',
            'type' => [
                'required',
                'string',
                Rule::in([
                    Device::COMPUTER_TYPE,
                    Device::EXTRAS_TYPE,
                ]),
            ],
            'item' => [
                'required',
                'string',
                Rule::in([
                    Device::DESKTOP_ITEM,
                    Device::LAPTOP_ITEM,
                    Device::MONITOR_ITEM,
                    Device::KEYBOARD_ITEM,
                    Device::MOUSE_ITEM,
                    Device::WEBCAM_ITEM,
                    Device::MICROPHONE_ITEM,
                    Device::SOUND_ITEM,
                ])
            ],
            'brand' => 'nullable|string',
            'model' => 'nullable|string',
            'serial_number' => 'nullable|string',
            'requisition_id' => 'required|exists:App\Models\Requisition,id',
            'acquired' => 'boolean|nullable',
        ]);

        $values = $this->devicesVerboseHandler($validator->validated());
        Device::create([
            'requisition_id' => $values['requisition_id'],
            'type' => $values['type'],
            'item' => $values['item'],
            'brand' => $values['brand'],
            'model' => $values['model'],
            'serial_number' => $values['serial_number'],
            'ars_number' => $values['ars_number']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Requisition $requisition
     * @param Request $request
     * @return RequisitionResource
     */
    public function show(Requisition $requisition, Request $request): RequisitionResource
    {
        $user = $request->user();
        if ($user->id !== $requisition->user_id) {
            return abort(403, "SHOW - Não autorizado.");
        }
        return new RequisitionResource($requisition);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequisitionRequest $request
     * @param Requisition $requisition
     * @return RequisitionResource
     * @throws ValidationException
     */
    public function update(UpdateRequisitionRequest $request, Requisition $requisition): RequisitionResource/*Response*/
    {
        $data = $request->validated();

        //Update requisition in the database
        $requisition->update($data);

        // Get ids as plain array of existing devices
        $existingIds = $requisition->devices->pluck('id')->toArray();

        //Get ids in plain array of new devices
        $newIds = Arr::pluck($data['devices'], 'id');

        //Find devices to delete
        $toDelete = array_diff($existingIds, $newIds);

        //Find devices to add
        $toAdd = array_diff($newIds, $existingIds);
        
        //Delete devices by $toDelete array
        Device::destroy($toDelete);

        //Create new devices
        foreach ($data['devices'] as $device) {
            if (in_array($device['id'], $toAdd)) {
                $device['requisition_id'] = $requisition->id;
                $this->createDevice($device);
            }
        }

        //Update existing devices
        $deviceMap = collect($data['devices'])->keyBy('id');
        foreach ($requisition->devices as $device) {
            if (isset($deviceMap[$device->id])) {
                $this->updateDevice($device, $deviceMap[$device->id]);
            }
        }

        $requisition = Requisition::where('id', $requisition->id)->first();
        return new RequisitionResource($requisition);
    }

    /**
     * return bool
     * @throws ValidationException
     */
    private function updateDevice(Device $device, $data): void
    {
        $validator = Validator::make($data, [
            'id' => 'required|exists:App\Models\Device,id',
            'ars_number' => 'nullable|string',
            'type' => [
                'required',
                'string',
                Rule::in([
                    Device::COMPUTER_TYPE,
                    Device::EXTRAS_TYPE,
                ]),
            ],
            'item' => [
                'required',
                'string',
                Rule::in([
                    Device::DESKTOP_ITEM,
                    Device::LAPTOP_ITEM,
                    Device::MONITOR_ITEM,
                    Device::KEYBOARD_ITEM,
                    Device::MOUSE_ITEM,
                    Device::WEBCAM_ITEM,
                    Device::MICROPHONE_ITEM,
                    Device::SOUND_ITEM,
                ])
            ],
            'brand' => 'nullable|string',
            'model' => 'nullable|string',
            'serial_number' => 'nullable|string',
            //'requisition_id' => 'required|exists:App\Models\Requisition,id',
            'acquired' => 'boolean|nullable',
        ]);

        $values = $this->devicesVerboseHandler($validator->validated());

        $device->update($values);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Requisition $requisition
     * @param Request $request
     * @return Response
     */
    public function destroy(Requisition $requisition, Request $request): Response
    {
        $user = $request->user();
        if ($user->id !== $requisition->user_id) {
            return abort(403, 'DELETE - Não autorizado');
        }

        $requisition->devices()->delete();
        $requisition->delete();
        return response('', 204);
    }

    /**
     * @throws FileNotFoundException
     */
    public function generatePdf(string $code, Request $request, Response $response): Response
    {
        $requisition = Requisition::where('code', $code)->first();
        $user = $request->user();

        if (!$requisition)
            return abort(404, 'O recurso com o código ' . $code . ' não existe.');

        if (!$user)
            return abort(401, 'Não autorizado. Este utilizador não pode executar esta ação.');

        if ($user->id !== $requisition->user_id)
            return abort(403, 'Não tem acesso a este recurso.');


        $filename = $request->user()->num_mecanografico . "_" . $requisition->code . '@' . $requisition->created_at->format('Y-m-d') . '.pdf';
        $html = PDF::loadView('pdf/requisição/template', ['requisition' => $requisition]);
        $html->save(Storage::disk('requisitions')->path($filename));
        return $response->header('Content-Type', 'application/pdf')->setContent($html->output());
    }
}
