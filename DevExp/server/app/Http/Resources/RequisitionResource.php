<?php

namespace App\Http\Resources;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Exception;

/**
 *
 * Every resource class defines a toArray method which returns the array of
 * attributes that should be converted to JSON when the resource is returned
 * as a response from a route or controller method.
 *
 * @property mixed $expire_date
 * @property mixed $deliver_at
 * @property mixed $description
 * @property mixed $id
 * @property mixed $devices
 * @property mixed $user_id
 * @property mixed $status
 * @property mixed $code
 */
class RequisitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'status' => $this->status,
            'description' => $this->description,
            'deliver_at' => (new DateTime($this->deliver_at))->format('Y-m-d'),
            'expire_date' => (new DateTime($this->expire_date))->format('Y-m-d'),
            'devices' => DeviceResource::collection($this->devices),
        ];
    }
}
