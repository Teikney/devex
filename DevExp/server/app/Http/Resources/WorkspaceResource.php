<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $id
 * @property mixed $code
 * @property mixed $name
 * @property mixed $institution
 * @property mixed $unit
 */
class WorkspaceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "id" => $this->id,
            "code" => $this->code,
            "name" => $this->name,
            "institution" => [
                "id" => $this->institution->id,
                "type" => [
                    "id" => $this->institution->institution_type->id,
                    "initials" => $this->institution->institution_type->initials,
                    "name" => $this->institution->institution_type->name,
                ],
                "county" => [
                    "id" => $this->institution->county->id,
                    "code" => $this->institution->county->code,
                    "name" => $this->institution->county->name,
                    "zone" => $this->institution->county->zone,
                ]
            ],
            "unit" => [
                "id" => $this->unit->id,
                "name" => $this->unit->name,
                "initials" => $this->unit->initials,
            ]
        ];
    }
}
