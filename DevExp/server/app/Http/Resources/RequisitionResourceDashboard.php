<?php

namespace App\Http\Resources;

use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 * Every resource class defines a toArray method which returns the array of
 * attributes that should be converted to JSON when the resource is returned
 * as a response from a route or controller method.
 *
 * @property mixed $id
 * @property mixed $code
 * @property mixed $description
 * @property mixed $status
 * @property mixed $deliver_at
 * @property mixed $expire_date
 */
class RequisitionResourceDashboard extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function toArray($request) :array
    {
        return [
            'id' => $this->id,
            'code' => $this->code ?? '#000000000000',
            'description' => $this->description,
            'status' => $this->status,
            'deliver_at' => (new DateTime($this->deliver_at))->format('Y-m-d'),
            'expire_date' => (new DateTime($this->expire_date))->format('Y-m-d'),
        ];
    }
}
