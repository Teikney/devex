<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 * Every resource class defines a toArray method which returns the array of
 * attributes that should be converted to JSON when the resource is returned
 * as a response from a route or controller method.
 *
 * @property mixed $id
 * @property mixed $type
 * @property mixed $item
 * @property mixed $brand
 * @property mixed $model
 * @property mixed $serial_number
 * @property mixed $ars_number
 * @property mixed $acquired
 * @property mixed $requisition
 */
class DeviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'item' => $this->item,
            'brand' => $this->brand,
            'model' => $this->model,
            'serial_number' => $this->serial_number,
            'ars_number' => $this->ars_number,
            'acquired' => $this->acquired,
            'requisition_code' => $this->requisition->code
        ];
    }
}
