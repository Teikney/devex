<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(App\Models\Requisition::class,'requisition_id');
            $table->string('type',30);
            $table->string('item',30);
            $table->string('brand',30)->nullable();
            $table->string('model',200)->nullable();
            $table->string('serial_number',1000)->nullable();
            $table->string('ars_number',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
