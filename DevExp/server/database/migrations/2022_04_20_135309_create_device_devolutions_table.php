<?php

use App\Models;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceDevolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_devolutions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Models\Device::class, 'device_id');
            $table->foreignIdFor(Models\User::class, 'user_id');
            $table->foreignIdFor(Models\User::class, 'manager_id')->nullable();
            $table->tinyInteger('status');
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_devolutions');
    }
}
