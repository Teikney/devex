<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserResponsabilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_responsabilities', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class, 'user_id');
            $table->string('type');
            $table->string('brand');
            $table->string('model');
            $table->string('serial');
            $table->string('macAddress');
            $table->string('fname');
            $table->string('email');
            $table->string('cc')->nullable();
            $table->string('num_mecanografico');
            $table->string('phone');
            $table->string('bond');
            $table->string('unit');
            $table->string('institution');
            $table->date('endDate');
            $table->longText('justification');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_responsabilities');
    }
}
