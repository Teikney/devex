<?php

use App\Models;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceRequisitionDevolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_requisition_devolutions', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Models\Requisition::class,'device_requisition_id');
            $table->foreignIdFor(Models\DeviceDevolution::class,'devolution_manager_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_requisition_devolutions');
    }
}
