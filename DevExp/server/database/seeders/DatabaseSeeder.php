﻿<?php

use App\Models\Workspaces\County;
use App\Models\Workspaces\Institution;
use App\Models\Workspaces\InstitutionType;
use App\Models\Workspaces\Unit;
use App\Models\Workspaces\Workspace;
use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {

        $admin = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Administrador',
        ]);

        $manager = Bouncer::role()->firstOrCreate([
            'name' => 'manager',
            'title' => 'Gestor NSIC',
        ]);

        $user = Bouncer::role()->firstOrCreate([
            'name' => 'user',
            'title' => 'Funcionário',
        ]);

        $list_all = Bouncer::ability()->firstOrCreate([
            'title' => 'Listar Todos os Utilizadores',
            'name' => 'list_all',
        ]);

        $list_managers = Bouncer::ability()->firstOrCreate([
            'name' => 'list_managers',
            'title' => 'Listar Todos os Gestores NSIC',
        ]);
        $list_users = Bouncer::ability()->firstOrCreate([
            'name' => 'list_users',
            'title' => 'Listar Todos os Funcionários',
        ]);
        $manage_admins = Bouncer::ability()->firstOrCreate([
            'name' => 'manage_admins',
            'title' => 'Gerir Administradores',
        ]);
        $manage_managers = Bouncer::ability()->firstOrCreate([
            'name' => 'manage_managers',
            'title' => 'Gerir Gestores NSIC',
        ]);
        $manage_users = Bouncer::ability()->firstOrCreate([
            'name' => 'manage_users',
            'title' => 'Gerir Utilizadores',
        ]);
        $approve_requisition = Bouncer::ability()->firstOrCreate([
            'name' => 'approve_requisition',
            'title' => 'Aprovar Requisição',
        ]);
        $see_others_devices = Bouncer::ability()->firstOrCreate([
            'name' => 'see_others_devices',
            'title' => 'Ver Equipamentos de Todos os Funcionários',
        ]);
        $create_requisition = Bouncer::ability()->firstOrCreate([
            'name' => 'create_requisition',
            'title' => 'Criar Requisição',
        ]);
        $show_requisition = Bouncer::ability()->firstOrCreate([
            'name' => 'show_requisition',
            'title' => 'Ver Requisição',
        ]);
        $update_requisition = Bouncer::ability()->firstOrCreate([
            'name' => 'update_requisition',
            'title' => 'Atualizar Requisição',
        ]);
        $close_requisition = Bouncer::ability()->firstOrCreate([
            'name' => 'close_requisition',
            'title' => 'Fechar Requisição',
        ]);
        $see_their_devices = Bouncer::ability()->firstOrCreate([
            'name' => 'see_their_devices',
            'title' => 'Ver os Seus Equipamentos',
        ]);

        $create_responsibility_terms = Bouncer::ability()->firstOrCreate([
            'name' => 'create_responsibility_terms',
            'title' => 'Criar Termos de Responsabilidade',
        ]);


        $see_others_responsibility_terms = Bouncer::ability()->firstOrCreate([
            'name' => 'see_others_responsibility_terms',
            'title' => 'Ver Todos os Termos de Responsabilidade',
        ]);

        $see_their_responsibility_terms = Bouncer::ability()->firstOrCreate([
            'name' => 'see_their_responsibility_terms',
            'title' => 'Ver os Seus Termos de Responsabilidade',
        ]);

        Bouncer::allow($admin)->to($list_all);
        Bouncer::allow($admin)->to($list_managers);
        Bouncer::allow($admin)->to($list_users);
        Bouncer::allow($admin)->to($manage_admins);
        Bouncer::allow($admin)->to($manage_managers);
        Bouncer::allow($admin)->to($manage_users);
        Bouncer::allow($admin)->to($create_requisition);
        Bouncer::allow($admin)->to($show_requisition);
        Bouncer::allow($admin)->to($update_requisition);
        Bouncer::allow($admin)->to($close_requisition);
        Bouncer::allow($admin)->to($see_their_devices);
        Bouncer::allow($admin)->to($see_others_devices);
        Bouncer::allow($admin)->to($approve_requisition);
        Bouncer::allow($admin)->to($create_responsibility_terms);
        Bouncer::allow($admin)->to($see_their_responsibility_terms);
        Bouncer::allow($admin)->to($see_others_responsibility_terms);
        Bouncer::allow($manager)->to($create_requisition);
        Bouncer::allow($manager)->to($show_requisition);
        Bouncer::allow($manager)->to($update_requisition);
        Bouncer::allow($manager)->to($close_requisition);
        Bouncer::allow($manager)->to($see_their_devices);
        Bouncer::allow($manager)->to($see_others_devices);
        Bouncer::allow($manager)->to($create_responsibility_terms);
        Bouncer::allow($manager)->to($see_their_responsibility_terms);
        Bouncer::allow($manager)->to($see_others_responsibility_terms);
        Bouncer::allow($user)->to($create_requisition);
        Bouncer::allow($user)->to($show_requisition);
        Bouncer::allow($user)->to($update_requisition);
        Bouncer::allow($user)->to($close_requisition);
        Bouncer::allow($user)->to($see_their_devices);
        Bouncer::allow($user)->to($create_responsibility_terms);
        Bouncer::allow($user)->to($see_their_responsibility_terms);


        //Units
        Unit::truncate();
        $file = fopen(database_path("seeders/data/units.csv"), "r");
        $headers = fgetcsv($file, 1000);
        while (($data = fgetcsv($file, 1000)) !== FALSE) {
            Unit::create(array_combine($headers, $data));
        }
        fclose($file);
        echo "Units seeded.\r\n";


        //InstitutionType
        InstitutionType::truncate();
        $file = fopen(database_path("seeders/data/institution_types.csv"), "r");
        $headers = fgetcsv($file, 1000);
        while (($data = fgetcsv($file, 1000)) !== FALSE) {
            InstitutionType::create(array_combine($headers, $data));
        }
        fclose($file);
        echo "InstitutionTypes seeded.\r\n";


        //County
        County::truncate();
        $file = fopen(database_path("seeders/data/counties.csv"), "r");
        $headers = fgetcsv($file, 1000);
        while (($data = fgetcsv($file, 1000)) !== FALSE) {
            County::create(array_combine($headers, $data));
        }
        fclose($file);

        echo "Counties seeded.\r\n";


        //Institution
        Institution::truncate();
        $file = fopen(database_path("seeders/data/institutions.csv"), "r");
        $headers = fgetcsv($file, 1000);
        while (($data = fgetcsv($file, 1000)) !== FALSE) {
            Institution::create(
                array_combine($headers, [
                    intval($data[0]),
                    InstitutionType::where('id', $data[1])->first()->id,
                    County::where('id', $data[2])->first()->id,
                ])
            );
        }
        fclose($file);
        echo "Institutions seeded.\r\n";


        //Workspaces
        Workspace::truncate();
        $file = fopen(database_path("seeders/data/workspaces.csv"), "r");
        $headers = fgetcsv($file, 1000);
        while (($data = fgetcsv($file, 1000)) !== FALSE) {
            Workspace::create(
                array_combine($headers, [
                    intval($data[0]),
                    Institution::where('id', intval($data[1]))->first()->id,
                    $data[2],
                    Unit::where('id', intval($data[3]))->first()->id,
                    $data[4]
                ])
            );
        }
        fclose($file);
        echo "Workspaces seeded.\r\n";

        file_put_contents(
            storage_path("app/data/workspaces.json"),
            json_encode(
                Unit::select('id','name')->get()
                    /*->join('workspaces','workspaces.unit_id','units.id')
                    ->whereHas('workspace', function ($query) {
                        $query->select('*')->where('institution_id', 1);
                    })->get()*/,
                JSON_UNESCAPED_UNICODE));

    }
}
