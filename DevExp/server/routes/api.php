<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->group(function() {
    Route::post('/logout', [Controllers\AuthController::class, 'logout']);
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::get('/docs/requisition/pdf/{requisition:code}',[Controllers\RequisitionController::class,'generatePdf'])->name('pdfgen');
    Route::resource('/requisitions',Controllers\RequisitionController::class);
    Route::get('/responsible/user/workspaces', [Controllers\UserResponsibleController::class,'getWorkspaces']);
    Route::resource('/responsible/user', Controllers\UserResponsibleController::class);
    Route::resource('/devices',Controllers\DeviceController::class);
    Route::get('/dashboard', [Controllers\DashboardController::class,'index']);
    Route::get('/abilities', [Controllers\DashboardController::class,'getAbilities']);
});
Route::get('/', [Controllers\ApiController::class,'index']);
Route::post('/register', [Controllers\AuthController::class,'register']);
Route::post('/login', [Controllers\AuthController::class, 'login']);
//Route::get('/docs/requisition/pdf/{requisition:code}',[Controllers\RequisitionController::class,'generatePdf']);
