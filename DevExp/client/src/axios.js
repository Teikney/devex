import axios from "axios";
import store from "./store";


const axiosClient = axios.create({
    baseURL: `http://10.11.29.191/api`
})

axiosClient.interceptors.request.use(
    config => {
        config.headers.Authorization = `Bearer ${store.state.user.token}`;

        config.headers.Accept = 'application/json';
        return config;
    }
)

export default axiosClient;
