import {createRouter, createWebHistory} from "vue-router"
import store from "../store";

const routes = [
    {
        path: '/',
        redirect: '/dashboard',
        component: () => import('../components/DefaultLayout.vue'),
        meta: {
            requiresAuth: true,
            isAdmin: false,
        },
        children: [
            {
                path: '/dashboard',
                name: 'Dashboard',
                component: () => import('../views/Dashboard.vue'),
                children: [
                    {
                        path: '/admin',
                        name: 'AdminDashboard',
                        component: () => import('../views/AdminDashboard.vue'),
                        meta: {
                            isAdmin:true,
                        }
                    }
                ]
            },
            {path: '/requisitions', name: 'Requisitions', component: () => import('../views/Requisitions.vue')},
            {path: '/requisitions/create', name: 'RequisitionCreate', component: () => import('../views/RequisitionView.vue')},
            {path: '/requisitions/:id', name: 'RequisitionView', component: () => import('../views/RequisitionView.vue')},
            {path: '/devices', name: 'Device', component: () => import('../views/Devices.vue')},
            {path: '/devices/create', name: 'DeviceView', component: () => import('../views/DeviceView.vue')},
            {path: '/responsabilities', name: 'Responsabilities', component: () => import('../views/Responsibilities.vue')},
            {path: '/responsabilities/create', name: 'ResponsabilityCreate', component: () => import('../views/ResponsibilityView.vue')},
            {path: '/responsabilities/:id', name: 'ResponsabilityView', component: () => import('../views/ResponsibilityView.vue')},
        ]
    },
    {
        path: '/auth',
        redirect: '/login',
        name: 'Auth',
        component: () => import('../components/AuthLayout.vue'),
        meta: {isGuest: true},
        children: [
            {
                path: '/login',
                name: 'Login',
                component: () => import('../views/Login.vue'),

            },{
                path: '/register',
                name: 'Register',
                component: () => import('../views/Register.vue'),
            },
        ]
    },
    { path: '/:pathMatch(.*)*', component: () => import('../components/PathNotFound.vue')},
]

const router = createRouter({
    history: createWebHistory(),
    routes
})


router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth && !store.state.user.token) {
        next({name:'Login'})

    } else if(store.state.user.token && to.meta.isGuest) {
        next({name:'Dashboard'})
    } else {
        next()
    }
})

export default router;
