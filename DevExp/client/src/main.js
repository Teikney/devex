import {createApp} from 'vue'
import store from "./store";
import router from "./router";
import './index.css'

import App from './App.vue';
import {abilitiesPlugin} from "@casl/vue";
import ability from "./services/ability";

createApp(App)
    .use(store)
    .use(router)
    .use(abilitiesPlugin,ability)
    .mount('#app')
