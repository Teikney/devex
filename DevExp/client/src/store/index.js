import {createStore} from 'vuex';
import axiosClient from "../axios";

const store = createStore({
    state: {
        user: {
            data: {
                role_id: null,
            },
            token: localStorage.getItem('TOKEN'),
            id: {
                num: localStorage.getItem('num') ?? '',
                role: localStorage.getItem('role') ?? ''

            },
            abilities: [
                'allRequisitions',
                'acceptRequisitions',
                'ownedRequisitions',
            ]
        },
        roles: [
            {code: '12digitsCodeFromBackEnd', name: 'Funcionário'},
            {code: '212digitsCodeFromBackEnd', name: 'GestorNSIC'},
            {code: '112digitsCodeFromBackEnd', name: 'Administrador'}

        ],
        deviceTypes: [
            {text: 'computador', id: "1"},
            {text: 'acessórios', id: "2"},
        ],
        deviceItems: {
            1: [
                {text: "fixo", id: "1"},
                {text: "portátil", id: "2"}
            ],
            2: [
                {text: "monitor", id: "3"},
                {text: "teclado", id: "4"},
                {text: "rato", id: "5"},
                {text: "câmara", id: "6"},
                {text: "microfone", id: "7"},
                {text: "colunas", id: "8"}
                ,]
        },
        devices: {
            loading: false,
            links: [],
            data: [],
        },
        currentRequisition: {
            loading: false,
            data: {}
        },
        currentResponsibility: {
            loading: false,
            data: {}
        },
        requisitions: {
            loading: false,
            links: [],
            data: [],
        },
        notification: {
            show: false,
            type: null,
            message: null,
        },
        dashboard: {
            loading: false,
            data: {
                latest_devices: [],
                latest_requisition: null,
                total_requisitions: 0,
                total_devices: 0,
            },
            devicesVerbose: "",
            devicesNames: []
        },
        responsabilities: {
            loading: false,
            data: {},
        },
        workspaces: [],
        departments: [],
        services: {},
    },
    getters: {},
    actions: {
        saveResponsibilities({commit}, responsibility) {
            commit('responsibilitiesLoading', true);
            if (responsibility.id) {
                return axiosClient.put(`/responsible/user/${responsibility.id}`, responsibility)
                    .then((response) => {
                        commit("setCurrentResponsability", response.data);
                        return response;
                    })
                    .catch((errors) => {
                        //commit responsability errors
                        return errors;
                    })
            } else {
                return axiosClient
                    .post(`/responsible/user`, responsibility)
                    .then((response) => {
                        commit("setCurrentResponsibility", response.data);
                        return response;
                    })
                    .catch((errors) => {
                        //commit responsability errors
                        return errors;
                    })
            }
        },
        getWorkspaces({commit}) {
            return axiosClient.get(`responsible/user/workspaces`)
                .then((response) => {
                    commit('workspacesData', response.data);
                })
                .catch((errors) => {
                    return errors;
                })
        },
        getResponsabilities({commit}) {
            commit('responsibilitiesLoading', true);
            return axiosClient.get(`/responsible/user`)
                .then((response) => {
                    commit('responsabilitiesData', response.data);
                    commit('responsibilitiesLoading', false);
                })
                .catch((errors) => {
                    commit('responsibilitiesLoading', false);
                    return errors;
                })
        },
        getDashboardData({commit}) {
            commit('dashboardLoading', true);
            return axiosClient.get(`/dashboard`)
                .then((res) => {
                    commit('setDashboardData', res.data)
                    commit('dashboardDevicesVerbose', res.data)
                    commit('dashboardDevicesNames', res.data)
                    commit('dashboardLoading', false);
                    return res;
                })
                .catch((err) => {
                    commit('dashboardLoading', false)
                    return err;
                })
        },
        getDevices({commit}, {url = null} = {}) {
            url = url || '/devices';
            commit('setDevicesLoading', true);
            return axiosClient
                .get(url)
                .then((res) => {
                    commit('setDevicesLoading', false)
                    commit('setDevices', res.data);
                    return res;
                })
        },
        getRequisition({commit}, id) {
            commit("setCurrentRequisitionLoading", true);
            return axiosClient
                .get(`/requisitions/${id}`)
                .then((res) => {
                    commit("setCurrentRequisition", res.data);
                    commit("setCurrentRequisitionLoading", false);
                    return res;
                })
                .catch((err) => {
                    commit("setCurrentRequisitionLoading", false);
                    throw err;
                });
        },
        saveRequisition({commit}, requisition) {
            //delete requisition.image_url;
            let response;
            if (requisition.id) {
                response = axiosClient
                    .put(`/requisitions/${requisition.id}`, requisition)
                    .then((res) => {
                        commit("setCurrentRequisition", res.data);
                        return res;
                    })
            } else {
                response = axiosClient
                    .post("/requisitions", requisition)
                    .then((res) => {
                        commit("setCurrentRequisition", res.data);
                        return res;
                    })
            }

            return response;
        },
        deleteRequisition({}, id) {
            return axiosClient.delete(`/requisitions/${id}`)
        },
        previewRequisition({commit}, code) {
            return axiosClient.get(`/docs/requisition/pdf/${code}`, {
                headers: {'Accept': 'application/pdf'},
                responseType: 'arraybuffer'
            })
                .then(response => {
                    const file = new Blob([response.data], {type: 'application/pdf'});
                    const fileUrl = URL.createObjectURL(file)
                    window.open(fileUrl, '_blank');
                }).catch((errorMsg) => console.log(errorMsg))
        },
        getRequisitions({commit}, {url = null} = {}) {
            url = url || '/requisitions';
            commit('setRequisitionsLoading', true);
            return axiosClient
                .get(url)
                .then((res) => {

                    commit('setRequisitionsLoading', false);
                    commit('setRequisitions', res.data);
                    return res;
                })
        },
        getRequisitionBySlug({commit}, slug) {
            commit("setCurrentRequisitionLoading", true);
            return axiosClient.get(`/requisition-by-slug/${slug}`)
                .then((res) => {
                    commit("setCurrentRequisition", res.data)
                    commit("setCurrentRequisitionLoading", false)
                    return res;
                })
                .catch((err) => {
                    commit("setCurrentRequisitionLoading", false)
                    throw err;
                })
        },
        saveRequisitionAnswer({commit}, {requisitionId, answers}) {
            return axiosClient.post(`/requisitions/${requisitionId}/answer`, {answers})
        },
        register({commit}, user) {
            return axiosClient.post('/register', user)
                .then(({data}) => {
                    commit('setUser', data);
                    return data;
                })
        },
        login({commit}, user) {
            return axiosClient.post('/login', user)
                .then(({data}) => {
                    commit('setUser', data);
                    return data;
                })
        },
        logout({commit}) {
            return axiosClient.post('/logout', store.state.user.token)
                .then(response => {
                    commit('logout')
                    return response
                })
        }
    },
    mutations: {
        setCurrentResponsibility: (state,responsibility) => {
            state.currentResponsibility = responsibility.data;
        },
        workspacesData: (state,workspaces) => {
            state.workspaces = workspaces.data;
            state.services = {};
            state.departments = [];

            workspaces.data.forEach((workspace) => {

                if (!state.departments.some(obj => obj.id === workspace.unit.id)) {
                    //store the units
                    state.departments.push(workspace.unit)
                }
                if (state.services[workspace.unit.id] === undefined) {
                    state.services[workspace.unit.id] = []
                }
                state.services[workspace.unit.id].push({id: workspace.id, name:workspace.name})
            })
        },
        responsabilitiesData: (state,user_responsibilities) => {
            state.responsabilities.data = user_responsibilities.data;
        },
        responsibilitiesLoading: (state, loading) => {
            state.responsabilities.loading = loading;
        },
        saveRequisition: (state, requisition) => {
            state.requisitions.data = [...state.requisitions, requisition.data];
        },
        updateRequisition: (state, requisition) => {
            state.requisitions.data = state.requisitions.map((r) => {
                if (r.id === requisition.data.id) {
                    return requisition.data
                }
            })
        },
        setDevices: (state, devices) => {
            state.devices.links = devices.meta.links;
            state.devices.data = devices.data;
        },
        setDevicesLoading: (state, loading) => {
            state.devices.loading = loading;
        },
        setDashboardData: (state, data) => {
            state.dashboard.data = data;
        },
        dashboardLoading: (state, loading) => {
            state.dashboard.loading = loading;
        },
        dashboardDevicesVerbose: (state, data) => {
            function upperCaseFirst(str) {
                return str.charAt(0).toUpperCase() + str.slice(1);
            }

            state.dashboard.devicesVerbose = ""

            data.latest_devices.forEach(device => {
                state.deviceItems[device.type].forEach((item) => {
                    item.id === device.item
                        ? (device.type === '1'
                                ? state.dashboard.devicesVerbose += upperCaseFirst(state.deviceTypes[device.type - 1].text) + " " + upperCaseFirst(item.text) + ", "
                                : state.dashboard.devicesVerbose += upperCaseFirst(item.text) + ", "
                        ) : ""
                })
            })
            state.dashboard.devicesVerbose = state.dashboard.devicesVerbose.replace(/,.$/, ".")
            //console.log(state.dashboard.devicesVerbose)

            //state.dashboard.devicesVerbose = ""
            //for(let i = 0; i < data.latest_devices.length; i++) {
            //    for (let type in state.deviceTypes) {
            //        if ((parseInt(type) + 1) === parseInt(data.latest_devices[i].type)) {
            //            for (let item in state.deviceItems[(parseInt(type) + 1)]) {
            //                if ((parseInt(item) + 1) === parseInt(data.latest_devices[i].item)) {
            //                    //console.log((parseInt(type) + 1),(parseInt(item) + 1))
            //                    //console.log(state.deviceItems[(parseInt(type) + 1)])
            //                    for (let j = 0; j < state.deviceItems[(parseInt(type) + 1)].length; j++) {
            //                        state.dashboard.devicesVerbose += state.deviceItems[(parseInt(type) + 1)][j].id === data.latest_devices[i].item ? state.deviceItems[(parseInt(type) + 1)][j].text + ', ' : ''
            //                    }
            //                    break
            //                }
            //            }
            //            break
            //        }
            //    }
            //}
            //state.dashboard.devicesVerbose = state.dashboard.devicesVerbose.replace(/,.$/,".")
            //console.log(state.dashboard.devicesVerbose)
        },
        dashboardDevicesNames: (state, data) => {
            function upperCaseFirst(str) {
                return str.charAt(0).toUpperCase() + str.slice(1);
            }

            state.dashboard.devicesNames = []
            data.latest_devices.forEach(device => {
                state.deviceItems[device.type].forEach((item) => {
                    let type = state.deviceTypes[device.type - 1].text
                    let afterType = item.text

                    //item.id === device.item
                    //    ? (device.type === '1'
                    //        ? state.dashboard.devicesNames.push(upperCaseFirst(type) + " " + upperCaseFirst(afterType))
                    //        : state.dashboard.devicesNames.push(upperCaseFirst(afterType))
                    //    ) : null
                    if (item.id === device.item) {
                        if (device.type === '1') {
                            let verbose = upperCaseFirst(type) + " " + upperCaseFirst(afterType)
                            data.latest_devices.forEach(equip => {
                                //console.log(equip, device)
                                if (equip === device && device.ars_number) {
                                    return verbose += ' @' + device.ars_number
                                }
                            }, device.index)
                            state.dashboard.devicesNames.push(verbose)
                        } else {
                            state.dashboard.devicesNames.push(upperCaseFirst(afterType))
                        }
                    }
                })

            })
        },
        setRequisitionsLoading: (state, loading) => {
            state.requisitions.loading = loading;
        },
        setRequisitions: (state, requisitions) => {
            state.requisitions.links = requisitions.meta.links;
            state.requisitions.data = requisitions.data;
        },
        setCurrentRequisition: (state, requisition) => {
            console.log(requisition.data.devices)
            state.currentRequisition.data = requisition.data;
            console.log(state.currentRequisition.data.devices)
        },
        setCurrentRequisitionLoading: (state, loading) => {
            state.currentRequisition.loading = loading;
        },
        logout: (state) => {
            state.user.token = null;
            state.user.role = null;
            state.user.data = {};
            localStorage.removeItem('num');
            localStorage.removeItem('role');
            localStorage.removeItem('TOKEN');
        },
        setUser: (state, userData) => {
            state.user.token = userData.token;
            state.user.data = userData.user;
            state.user.id = {
                user: userData.user.num_mecanografico,
                role: userData.user.role,
            };
            localStorage.setItem('num', userData.user.num_mecanografico);
            localStorage.setItem('role', userData.user.role_id !== 1 ? (userData.user.role_id === 2 ? 'manager' : 'user') : 'admin');
            localStorage.setItem('TOKEN', userData.token)

        },
        notify: (state, {message, type}) => {
            state.notification.show = true;
            state.notification.type = type;
            state.notification.message = message;
            setTimeout(() => {
                state.notification.show = false;
            }, 3000);
        }

    },
    modules: {},
});

export default store;
