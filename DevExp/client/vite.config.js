import {defineConfig, loadEnv} from 'vite'
import vue from '@vitejs/plugin-vue'
const path = require('path')

// https://vitejs.dev/config/
export default ({ mode }) => {
    process.env = {...process.env, ...loadEnv(mode,process.cwd())}
    return defineConfig({
        resolve: {
            alias: {
                '@' : path.resolve(__dirname, './src')

            }
        },
        plugins: [vue()],
        server: {
            host:process.env.VITE_API_HTTP_URL,
        },
        build: {
            sourcemap: false,
            resolve: {
                alias: {
                    '@' : path.resolve(__dirname, './src')
                }
            },

            // generate manifest.json in outDir
            manifest: true,
            emptyOutDir: true,
            rollupOptions: {
                // overwrite default .html entry
                //input: './src/main.js'
                inlineDynamicImports: true,
                output: {
                    entryFileNames: "[name].js",
                    chunkFileNames: "[name].js",
                    assetFileNames: "[name].[ext]",
                }
            }
        }
    })
}

